<!--/**
   ***Affichage des photos du dossier selectionné pour un patient donné
   **/
-->
<div class="row">
<?php 
	/*recuperation des photos du dossier du patient*/
	 $query = " SELECT IDPHOTO,IPP,NOMPHOTO,AUTEUR,DESCRIPTION,NOM_PATH,POSO,DUREE,NOM_MEDIC,TO_CHAR(DATESTOCKAGE,'DD/MM/YYYY') as DATESTCK,VISIBLE,IDDOS FROM TBLPHOTOS WHERE IPP='".$ipp."' AND IDDOS = '".$iddos."'INNER JOIN PATHOLOGIE ON TBLPHOTOS.id = PATHOLOGIE.id INNER JOIN TRAITEMENT ON PATHOLOGIE.id = TRAITEMENT.id INNER JOIN MEDICAMENT ON TRAITEMENT.idtrait = MEDICAMENT.ref ORDER BY IDPHOTO DESC";
     $answer = $connexion-> prepare($query);
     $answer ->execute();
     
     $nbsearch=0;
   /*affichage des photos avec leurs descritption, date d'ajout, auteur, pathologie, traitement*/
      while ($row =$answer-> fetch()) {
		  $nbsearch++;
     	if($row['VISIBLE']==1){
     		echo '<div class="gallery" >';
     		echo '<img src="'.$row['NOMPHOTO'].'" onclick="openModal();currentSlide('.$nbsearch.')" class="hover-shadow cursor" alt="photo" >';
     		echo '<div class="desc"><p><u>Description :</u> '.$row['DESCRIPTION'].'</p>';
     		echo '<p>Ajouté le '.$row['DATESTCK'];
                echo '<p>Pathologie: '.$nom_path;
                echo '<p>Traitement:  '.$traitement;
     		echo ' par '.$row['AUTEUR'].'</p>';
     		echo '</div>';
			echo '</div>';
     	}
     	
      }
	  if($nbsearch==0){
		  /* Verification de l'IPP*/
		if($tagipp != 0){
			/* IDDOS doit etre fourni dans le lien meme si il est vide*/
			//verif iddos
			 $query = " SELECT * FROM TBLDOSPHOTOS WHERE IDDOS=".$iddos;
			 $answer = $connexion-> prepare($query);
			$answer ->execute();
			 $nbsearch=0;
			  while ($row = $answer->fetch()) {
			  $nbsearch++;}
			if ( $nbsearch == 0 ){
				/* Cas ou il n'y a pas de dossier séléctionné*/
				echo '<center><img src="./images/Aucun_dossier.png" alt="LOGO"></center>';
			}else{
				/* Cas ou le dossier séléctioné est vide*/
				echo '<center><img src="./images/Aucune_photo.png" alt="LOGO"></center>';
			}
		}else { 
		/* Cas ou le numéro IPP n'est pas valide*/
			echo '<center><img src="./images/Mauvais_ipp.png" alt="LOGO"></center>';
			}
		}
	  
	  ?>

  
</div>
<!--modal pour affichage de la photo selctionnée-->
<div id="myModal" class="modal">
  <span class="close cursor" onclick="closeModal()">&times;</span>
  <!--Affichage de la photo-->
  <div class="modal-content">
	<?php $query = "SELECT * FROM TBLPHOTOS WHERE IPP='".$ipp."' AND IDDOS = '".$iddos."' ORDER BY IDPHOTO DESC";
     $answer = $connexion-> prepare($query);
    $answer ->execute();
     
     $nbsearch=0;
   
      while ($row =$answer->fetch()) {
     	if($row['VISIBLE']==1){
     		echo '<div class="mySlides" >';
     		echo '<img src="'.$row['NOMPHOTO'].'" style="width:100%">';
			echo '</div>';
     	}
     	$nbsearch++;
		
		
      }?>
    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>
	
	<!--miniatures de toutes les photos du dossier-->
    <div class="caption-container">
      <p id="caption"></p>
    </div>

	<?php $query = "SELECT * FROM TBLPHOTOS WHERE IPP='".$ipp."' AND IDDOS = '".$iddos."' ORDER BY IDPHOTO DESC";
     $answer = $connexion-> prepare($query);
     $answer ->execute();
     
     $nbsearch=0;
   
      while ($row = $answer->fetch()) {
		  $nbsearch++;
     	if($row['VISIBLE']==1){
     		echo '<div class="column" >';
     		echo '<img class="demo cursor" src="'.$row['NOMPHOTO'].'" style="width:100%" onclick="currentSlide('.$nbsearch.')" alt="'.$row['DESCRIPTION'].'" >';
			echo '</div>';
     	}
     	
      }?>

  </div>
</div>
<!--Script de gestion de la modal-->
<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>

