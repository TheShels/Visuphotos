-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 28 mai 2019 à 10:33
-- Version du serveur :  5.7.23
-- Version de PHP :  7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `visuphotos`
--

-- --------------------------------------------------------

--
-- Structure de la table `medicament`
--

DROP TABLE IF EXISTS `medicament`;
CREATE TABLE IF NOT EXISTS `medicament` (
  `ref` int(11) NOT NULL,
  `NOM_MEDI` varchar(20) COLLATE utf8_bin NOT NULL,
  `DOSE` varchar(10) COLLATE utf8_bin NOT NULL,
  `TYPE` varchar(20) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ref`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `medicament`
--

INSERT INTO `medicament` (`ref`, `NOM_MEDI`, `DOSE`, `TYPE`) VALUES
(589, 'Aspirine', '50mg', 'Cachet');

-- --------------------------------------------------------

--
-- Structure de la table `pathologie`
--

DROP TABLE IF EXISTS `pathologie`;
CREATE TABLE IF NOT EXISTS `pathologie` (
  `ID_PATH` int(11) NOT NULL AUTO_INCREMENT,
  `NOM_PATH` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`ID_PATH`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `pathologie`
--

INSERT INTO `pathologie` (`ID_PATH`, `NOM_PATH`) VALUES
(1, 'Mal de tête');

-- --------------------------------------------------------

--
-- Structure de la table `pa_pat`
--

DROP TABLE IF EXISTS `pa_pat`;
CREATE TABLE IF NOT EXISTS `pa_pat` (
  `PAT_IPP` varchar(12) COLLATE utf8_bin NOT NULL,
  `ETC_NOM` varchar(35) COLLATE utf8_bin NOT NULL,
  `ETC_NOM_MAR` varchar(35) COLLATE utf8_bin NOT NULL,
  `ETC_PRN` varchar(35) COLLATE utf8_bin NOT NULL,
  `ETC_DDN` date NOT NULL,
  `ETC_SEX` varchar(3) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`PAT_IPP`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `pa_pat`
--

INSERT INTO `pa_pat` (`PAT_IPP`, `ETC_NOM`, `ETC_NOM_MAR`, `ETC_PRN`, `ETC_DDN`, `ETC_SEX`) VALUES
('01', 'Mr', 'Exemple', 'premier', '2019-05-20', 'h');

-- --------------------------------------------------------

--
-- Structure de la table `tbldosphotos`
--

DROP TABLE IF EXISTS `tbldosphotos`;
CREATE TABLE IF NOT EXISTS `tbldosphotos` (
  `IDDOS` int(11) NOT NULL AUTO_INCREMENT,
  `IPP` varchar(12) COLLATE utf8_bin NOT NULL,
  `NOMDOS` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`IDDOS`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `tblphotos`
--

DROP TABLE IF EXISTS `tblphotos`;
CREATE TABLE IF NOT EXISTS `tblphotos` (
  `IDPHOTO` int(11) NOT NULL AUTO_INCREMENT,
  `IPP` varchar(12) COLLATE utf8_bin NOT NULL,
  `NOMPHOTO` varchar(400) COLLATE utf8_bin NOT NULL,
  `AUTEUR` varchar(200) COLLATE utf8_bin NOT NULL,
  `DESCRIPTION` varchar(400) COLLATE utf8_bin NOT NULL,
  `DATESTOCKAGE` date NOT NULL,
  `VISIBLE` int(11) NOT NULL DEFAULT '1',
  `IDDOS` int(11) NOT NULL,
  PRIMARY KEY (`IDPHOTO`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `tblutilisateur`
--

DROP TABLE IF EXISTS `tblutilisateur`;
CREATE TABLE IF NOT EXISTS `tblutilisateur` (
  `IDUTILISATEUR` varchar(10) COLLATE utf8_bin NOT NULL,
  `REFAD` varchar(200) COLLATE utf8_bin NOT NULL,
  `NOM` varchar(100) COLLATE utf8_bin NOT NULL,
  `PRENOM` varchar(100) COLLATE utf8_bin NOT NULL,
  `DROIT` int(11) NOT NULL DEFAULT '0',
  `ACTIF` varchar(1) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `UF` varchar(8) COLLATE utf8_bin NOT NULL,
  `TITRE` varchar(100) COLLATE utf8_bin NOT NULL,
  `LOGIN` varchar(50) COLLATE utf8_bin NOT NULL,
  `PHOTOS` varchar(1) COLLATE utf8_bin NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `tblutilisateur`
--

INSERT INTO `tblutilisateur` (`IDUTILISATEUR`, `REFAD`, `NOM`, `PRENOM`, `DROIT`, `ACTIF`, `UF`, `TITRE`, `LOGIN`, `PHOTOS`) VALUES
('1', 'CN=Lambert, Rodrigue, OU=Nominatifs, OU=Utilisateurs, OU=CHV, DC=ch, DC=net', 'LAMBERT', 'Rodrigue', 2, '1', '', 'Administrateur', 'rodrigue-l', '1');

-- --------------------------------------------------------

--
-- Structure de la table `traitement`
--

DROP TABLE IF EXISTS `traitement`;
CREATE TABLE IF NOT EXISTS `traitement` (
  `ID_TRAIT` int(11) NOT NULL AUTO_INCREMENT,
  `POSO` varchar(100) COLLATE utf8_bin NOT NULL,
  `DUREE` varchar(15) COLLATE utf8_bin NOT NULL,
  `DATE_PRESC` date NOT NULL,
  PRIMARY KEY (`ID_TRAIT`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `traitement`
--

INSERT INTO `traitement` (`ID_TRAIT`, `POSO`, `DUREE`, `DATE_PRESC`) VALUES
(1, '1 midi 1 soir', '15 jours', '2019-05-28');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
