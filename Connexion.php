<?php
//variable de connexion
$hostname = "localhost";
$port = "3306";
$user = "root";
$password = "";
$nom_base_donnees = "visuphotos";

//Connexion a la base de donnée
try
{
    $connexion = new PDO('mysql:host='.$hostname.';port='.$port.';dbname='.$nom_base_donnees, $user, $password, array (PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $connexion -> SetAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
} catch (Exception $e) 
{
    echo'ERREUR :'.$e->getMessage().'<br/>';
    echo'N : '.$e->getCode();
    exit();
}
?>

