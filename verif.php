<?php
   /**
   ***affichage des elements d'ajout de dossiers et d'image selon les droits de l'utilisateur
   **/

   /*verification des droits utilisateur*/
   $query = "SELECT * FROM TBLUTILISATEUR WHERE LOGIN ='".$user."' AND photos=1";
   $answer = $connexion-> prepare($query);
  $answer ->execute();
   $nbsearch = 0;
   
   while ($row = $answer->fetch()){
   	$nbsearch++;
	}
	
	   /*recuperation de l'id du dernier dossier pour envoi de l'id++ a la page pour affichage du dossier venant d'etre créé*/
	   $query3 = 'SELECT MAX(iddos) "ID" FROM TBLDOSPHOTOS';
	   $answer3 = $connexion-> prepare($query3);
	  $answer3 ->execute();
	   $nbsearch3 = 0;
	   $prochain_iddos = 0;
	   while ($row3 = $answer3->fetch()){
			$prochain_iddos = $row3[ID];
			$nbsearch3++;
		}
			
	$prochain_iddos++;
   /*si droits ok et bon ipp*/
   if($nbsearch > 0 && $tagipp != 0){
	   /*on affiche la création de dossier*/
   		echo '<form enctype="multipart/form-data" action="';
   		echo htmlspecialchars($_SERVER['PHP_SELF']."?person=".$ipp."&iddos=".$prochain_iddos."&user=".$user);
   		echo '" method="post" name="dossier">';
   		echo '<p>';
   		echo '<label for="dossier_a_creer" title="creation d un dossier">Créer dossier :</label>&nbsp;';
   		echo '<input type="text" name="nomdos" placeholder="nom du dossier" maxlength="30"/>';						
   		echo '&nbsp;&nbsp;<input class="bouton6" type="submit" name="submitDos" value="     Créer    " />';
   		echo '</p>';
   		echo '</form>';
   		
		/*on verifie qu'un dossier est selectionné*/ 
		$query2 = "SELECT * FROM TBLDOSPHOTOS WHERE iddos = ".$iddos;
		$answer2 = $connexion-> prepare($query2);
		$answer2 ->execute();
		$nbsearch2 = 0;
	    while ($row2 = $answer2->fetch()){
			$nbsearch2++;
		}
	   
	   /*on affiche l'ajout de photos seulement si un dossier est selectionné*/
	    if($nbsearch2 > 0){		
			echo '<form enctype="multipart/form-data" action="';
			echo htmlspecialchars($_SERVER['REQUEST_URI']);
			echo '" method="post">';
			echo '<p>';
			echo '<label for="fichier_a_uploader" title="Recherchez le fichier à uploader !">Ajouter une photo :</label>';
			echo '<input type="hidden" name="MAX_FILE_SIZE" value="'.MAX_SIZE.'"/>';
			echo '<input name="fichier" type="file" id="fichier_a_uploader" /> ';
			echo '<input type="text" name="description" placeholder="description" maxlength="255"/>';			
			echo '&nbsp;<input class="bouton6" type="submit" name="submit" value="   Insérer   " />';
			echo '</p>';
			echo '</form>';
			
			include 'QR.php';
			
			
		}
		
		
	}
	else {
			echo '<br>';
		}
	echo '<br>';
	/*afichage du champs de recherche de dossier*/
	echo '<input class="rechercher" type="text" id="mySearch" onkeyup="rechercheDossier()" placeholder="Filtrer les dossiers..." size="30" title="Filtrer les dossiers..." style = "margin-left : 8%"><br>';
?>